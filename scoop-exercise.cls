% This LaTeX class is intended to be used for exercise sheets.
\PassOptionsToPackage{fit}{truncate}
\ProvidesClass{scoop-exercise}

% Declare the options for the class.
% The class option 'noextras' suppresses 'scoopExtra' material.
\DeclareOption{noextras}{\PassOptionsToPackage{\CurrentOption}{scoop-teaching}}

% The class option 'nosolutions' suppresses 'scoopSolutions' to exercises.
\DeclareOption{nosolutions}{\PassOptionsToPackage{\CurrentOption}{scoop-teaching}}

% The class option 'final' implies 'noextras', 'nosolutions'.
\DeclareOption{final}{\PassOptionsToPackage{\CurrentOption}{scoop-teaching}}

% The class option 'showfilenames' suppresses the output of problem filenames.
\DeclareOption{showfilenames}{\PassOptionsToPackage{\CurrentOption}{scoop-teaching}}

% Pass all non-implemented options to the base class scrartcl.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}

% Process all options
\ProcessOptions\relax

% Load the base class scrartcl with default documentclass option english
% The implementation uses ideas from
% https://tex.stackexchange.com/questions/147243/a-class-with-default-language-settings.
\RequirePackage{etoolbox}
\preto\@classoptionslist{english,}
\LoadClass[a4paper, DIV = 12, parskip = half]{scrartcl}

% Resolve the dependencies of this package.
\RequirePackage{scoop-cls-commons}
\RequirePackage{scoop-translations}
\RequirePackage{chngcntr}
\RequirePackage{standalone}

% Configure the document class specific settings.
% Set document type.
\def\scoop@documenttype{\GetTranslation{Exercise}}

% Re-configure the problem and homework problem counters so that they are
% numbered independently of section.
\AtEndPreamble{\ifltxcounter{scoop@problem}{\counterwithout{scoop@problem}{section}}{}}
\AtEndPreamble{\ifltxcounter{scoop@homeworkproblem}{\counterwithout{scoop@homeworkproblem}{section}}{}}

% Set up the page header and footer.
\if@twoside
	\ihead*{\scoop@shortauthor}
	\ohead*{\scoop@shorttitle}
	\cfoot*{\url{\scoop@courseurl}}
	\ofoot*{\GetTranslation{page}~\thepage~\GetTranslation{of}~\lastpageref*{LastPage}}
\else%
	\ihead*{\scoop@shortauthor}
	\ohead*{\scoop@shorttitle}
	\cfoot*{\url{\scoop@courseurl}}
	\ofoot*{\GetTranslation{page}~\thepage~\GetTranslation{of}~\lastpageref*{LastPage}}
\fi

% Further customize the document's appearance.
\KOMAoptions{headlines=2}
\recalctypearea
