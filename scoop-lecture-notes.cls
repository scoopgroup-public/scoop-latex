% This LaTeX class is intended to be used for lecture notes.
\PassOptionsToPackage{fit}{truncate}
\ProvidesClass{scoop-lecture-notes}

% Declare the options for the class.
% The class option 'noextras' suppresses 'scoopExtra' material.
\DeclareOption{noextras}{\PassOptionsToPackage{\CurrentOption}{scoop-teaching}}

% The class option 'nosolutions' suppresses 'scoopSolutions' to exercises.
\DeclareOption{nosolutions}{\PassOptionsToPackage{\CurrentOption}{scoop-teaching}}

% The class option 'final' implies 'noextras', 'nosolutions'.
\DeclareOption{final}{\PassOptionsToPackage{\CurrentOption}{scoop-teaching}}

% Pass all non-implemented options to the base class scrreprt.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrreprt}}

% Process all options
\ProcessOptions\relax

% Load the base class scrreprt with default documentclass option english
% The implementation uses ideas from
% https://tex.stackexchange.com/questions/147243/a-class-with-default-language-settings.
\RequirePackage{etoolbox}
\preto\@classoptionslist{english,}
\LoadClass[a4paper, DIV = 12, parskip = half]{scrreprt}

% Resolve the dependencies of this package.
\RequirePackage{scoop-cls-commons}
\RequirePackage{scoop-translations}

% Configure the document class specific settings.
% Set document type.
\def\scoop@documenttype{\GetTranslation{Lecture Notes}}

% Remove \@date and spacing following it from \@maketitle
% https://tex.stackexchange.com/questions/288800/omit-the-date-in-maketitle-without-blanking-the-date
\patchcmd{\@maketitle}{{\usekomafont{date}{\@date \par}}\vskip \z@ \@plus 1em}{}{}{}

% Set up the page header and footer.
\if@twoside
	\rohead[]{\truncate{1.0\textwidth}{\ifx\scoop@shorttitle\empty{}\else{\scoop@shorttitle}\fi}}
	\chead[]{\scoop@license}
	\lehead[]{\truncate{1.0\textwidth}{\scoop@shortauthor}}
	\refoot[]{\@date}
	\cfoot*{\url{\scoop@courseurl}}
	\ofoot[]{\thepage}
\else
	\ohead[]{\truncate{0.45\textwidth}{\ifx\scoop@shorttitle\empty{}\else{\scoop@shorttitle}\fi}}
	\chead[]{\scoop@license}
	\ihead[]{\truncate{0.45\textwidth}{\scoop@shortauthor}}
	\ofoot*{\thepage}
	\cfoot*{\url{\scoop@courseurl}}
	\ifoot*{\@date}
\fi

% Further customize the document's appearance. Sections should be counted
% across chapters and numbered with § prefix.
\counterwithout{section}{chapter}
\renewcommand{\chapterformat}{\GetTranslation{Chapter}~\thechapter\hspace{1em}}
\renewcommand{\sectionformat}{§~\arabic{section}\hspace{1em}}
\renewcommand{\subsectionformat}{§~\arabic{section}.\arabic{subsection}\hspace{1em}}
\renewcommand{\subsubsectionformat}{§~\arabic{section}.\arabic{subsection}.\arabic{subsubsection}\hspace{1em}}
