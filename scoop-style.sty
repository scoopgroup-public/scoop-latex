% This LaTeX package sets a number of configuration options for the commonly
% used packages loaded through scoop-packages.sty.
\ProvidesPackage{scoop-style}

% Resolve the dependencies of this package.
\RequirePackage{ifthen}
\RequirePackage{ltxcmds}
\RequirePackage{scoop-colors}
\RequirePackage{scoop-translations}

% Define Boolean switches and set their default values.
\provideboolean{scoop@nextbranch}
\provideboolean{scoop@wrapcite}
\setboolean{scoop@wrapcite}{false}

% Configure the changes package.
\@ifpackageloaded{changes}{%
	% Determine whether the current .cls file has issues with \cite or with \cite[]
	% and requires to be wrapped into an \mbox inside \deleted.
	\@ifclassloaded{amsart}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{cocv}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifpackageloaded{dgruyter}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifpackageloaded{dgruyter-scoop}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{elsarticle}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{frontiersFPHY}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifpackageloaded{icml2020}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifpackageloaded{icml2021}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{ifacconf}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{ij4uq}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{ij4uq-scoop}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{imanum}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{interact}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{IEEEtran}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{m2an}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{scoop-preprint}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{sn-jnl}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{svjour3}{\@ifpackageloaded{natbib}{\setboolean{scoop@wrapcite}{true}}{}}{}
	\@ifclassloaded{WileyNJD-v2}{\setboolean{scoop@wrapcite}{true}}{}
	\@ifclassloaded{w-art}{\setboolean{scoop@wrapcite}{true}}{}
	% Improve the 'deleted' markup for the '\deleted' and '\replaced' commands.
	\newcommand{\scoop@deleted}[1]{%
		% Wrap the content of \cref into an \mbox to achieve compatibility with ulem.
		% The implementation uses ideas from
		% https://tex.stackexchange.com/questions/270901/cleveref-and-ulem-soul-compatibility,
		% http://mirrors.ctan.org/macros/latex/contrib/ulem/ulem.pdf.
		{\@ifpackageloaded{cleveref}{\let\oldcref\cref\renewcommand{\cref}[1]{\mbox{\oldcref{##1}}}}{}%
			\ifthenelse{\boolean{scoop@wrapcite}}{%
				\let\oldcite\cite\renewcommand{\cite}[2][]{\mbox{\oldcite[##1]{##2}}}%
			}{}
			% Locally increase the width of ulem's \sout-generated lines.
			\renewcommand{\ULthickness}{1.2pt}%
			% Use the current author's colors for all hyperref colors.
			\ltx@ifpackageloaded{hyperref}{\@ifundefinedcolor{authorcolor}{}{\hypersetup{allcolors=authorcolor}}}{}%
			% Fix the strikeout command for 'deleted' and 'replaced' commands to work correctly in displayed equations.
			% The implementation uses ideas from
			% https://tex.stackexchange.com/questions/297111/wrong-to-delete-replace-equation-by-using-changes-package.
			\ifmmode\text{\sout{\ensuremath{#1}}}\else\sout{#1}\fi%
		}%
	}
	% Apply the 'deleted' markup.
	\setdeletedmarkup{\scoop@deleted{#1}}
	% Fix the '\added' command to use current author's color for all hyperref colors.
	\newcommand{\scoop@added}[1]{%
		\ltx@ifpackageloaded{hyperref}{\@ifundefinedcolor{authorcolor}{}{\hypersetup{allcolors=authorcolor}}}{}%
		#1%
	}
	% Apply the 'added' markup.
	\setaddedmarkup{\scoop@added{#1}}
	% Setup the anonymous changes author.
	\@namedef{Changes@AuthorColor}{TolVibrantRed}
	\colorlet{Changes@Color}{TolVibrantRed}
	% Make the author markup upright, even in math mode.
	\renewcommand{\Changes@Markup@author}[1]{\textsuperscript{\textup{#1}}}
}{}

% Configure the biblatex package.
\@ifpackageloaded{biblatex}{%
	\ExecuteBibliographyOptions{
		sorting=nyt,             % sort by name, year, title
		url=true,                % do print urls
		doi=true,                % do print DOIs
		eprint=true,             % do show eprint information
		giveninits=true,         % always abbreviate first names of the authors
		uniquename=false,        % do not use initials even when different authors have the same last name
		maxcitenames=3,          % use ``et al'' when citing only with 4+ authors
		maxbibnames=99,          % use ``et al'' in the bibliography only with 100+ authors (i.e., never)
		mincitenames=1,          % when ``et al'' is used at cite time, abbreviate to one author only
		% dashed=false,            % do not use --- to replace author names in the bibliography (if the previous entry has the same authors)
		sortcites=false,         % do not sort if there are multiple keys in one \cite{}
		safeinputenc,            % for some accents, see https://tex.stackexchange.com/questions/170562
		isbn=false,              % do not shown ISBN, ISSN, ISRN
		useprefix=true,          % honor family name prefixes (de los Reyes)
		date=year,               % use only the year part of the date field
	}

	% Tweak the display to use comma in text but semicolon in bibliography.
	% The implementation uses ideas from
	% https://tex.stackexchange.com/questions/67621/biblatex-have-and-in-the-citation-but-in-the-bibliography
	\newcommand{\mybibcomma}{\ifbibliography{\addsemicolon}{\addcomma}}
	\renewcommand*{\nameyeardelim}{\addcomma\space}
	\renewcommand*{\multinamedelim}{\mybibcomma\space}
	\renewcommand*{\finalnamedelim}{\mybibcomma\space}
	\renewcommand*{\compcitedelim}{}
	\renewbibmacro{in:}{}

	% Make citation hyperlinks extend over the full citation text.
	% The implementation uses ideas from
	% https://tex.stackexchange.com/questions/1687/hyperlink-name-with-biblatex-authoryear
	% \DeclareCiteCommand{\cite}{%
	%   \ifbibmacroundef{cite:init}{}{\usebibmacro{cite:init}}\usebibmacro{prenote}%
	% }{%
	%   \usebibmacro{citeindex}%
	%   \printtext[bibhyperref]{\usebibmacro{cite}}%
	% }{%
	%   \ifbibmacroundef{cite:init}{\multicitedelim}{}%
	% }{%
	%   \usebibmacro{postnote}%
	% }%
	% \DeclareCiteCommand{\parencite}[\mkbibbrackets]{%
	%   \ifbibmacroundef{cite:init}{}{\usebibmacro{cite:init}}\usebibmacro{prenote}%
	% }{%
	%   \usebibmacro{citeindex}%
	%   \printtext[bibhyperref]{\usebibmacro{cite}}%
	% }{%
	%   \ifbibmacroundef{cite:init}{\multicitedelim}{}%
	% }{%
	%   \usebibmacro{postnote}%
	% }%

	% Make citation hyperlinks extend over the full citation text.
	% The implementation uses ideas from
	% https://tex.stackexchange.com/questions/15951/hyperlink-name-with-biblatex-authoryear-biblatex-1-4b/27107#27107
	\DeclareFieldFormat{citehyperref}{%
		\DeclareFieldAlias{bibhyperref}{noformat}\bibhyperref{#1}%
	}

	\DeclareFieldFormat{textcitehyperref}{%
		\DeclareFieldAlias{bibhyperref}{noformat}%
		\bibhyperref{%
			#1%
			\ifbool{cbx:parens}%
				{\bibcloseparen\global\boolfalse{cbx:parens}}%
			{}}%
		}

		\savebibmacro{cite}
		\savebibmacro{textcite}

		\renewbibmacro*{cite}{%
			\printtext[citehyperref]{\restorebibmacro{cite}\usebibmacro{cite}}%
		}

		\renewbibmacro*{textcite}{%
			\ifboolexpr{%
				( not test {\iffieldundef{prenote}} and test {\ifnumequal{\value{citecount}}{1}} )
				or
				( not test {\iffieldundef{postnote}} and test {\ifnumequal{\value{citecount}}{\value{citetotal}}} )
			}{%
				\DeclareFieldAlias{textcitehyperref}{noformat}
			}%
			{}%
			\printtext[textcitehyperref]{\restorebibmacro{textcite}\usebibmacro{textcite}}%
		}

	% Declare a format to typeset EPRINTTYPE = {HAL} entries.
	% The implementation uses ideas from
	% https://github.com/plk/biblatex/issues/38
	\DeclareFieldFormat{eprint:HAL}{HAL\addcolon\space\ifhyperref{\href{https://hal.archives-ouvertes.fr/#1}{\nolinkurl{#1}}}{\nolinkurl{#1}}}
}{}

% Configure the hyperref package.
\setboolean{scoop@nextbranch}{true}
\@ifclassloaded{beamer}{\setboolean{scoop@nextbranch}{false}}{}
\@ifclassloaded{siam220329}{\setboolean{scoop@nextbranch}{false}}{}
\@ifclassloaded{siamonline220329}{\setboolean{scoop@nextbranch}{false}}{}
\@ifpackageloaded{hyperref}{%
	\ifthenelse{\boolean{scoop@nextbranch}}{%
		\hypersetup{
			pdfpagemode=UseOutlines,  % directive for pdf file readers
			colorlinks=true,          % color links instead using boxes
			linkcolor=TolMutedBlue,   % internal links
			filecolor=TolMutedWine,   % local file URLs
			urlcolor=TolMutedWine,    % linked URLs
			citecolor=TolMutedGreen,  % citations
			runcolor=TolVibrantRed,   % run links
			plainpages=false,         % page anchors are always Arabic
			hypertexnames=false       % avoid duplicate destination names
		}
	}{}
}{}

% Configure the enumitem package.
\@ifpackageloaded{enumitem}{%
	% Make items and paragraphs inside an item equally separated by \parsep.
	\setlist{itemsep=0mm}
	% Explicitly set the value of \parsep for all list types.
	\setlist*{parsep=4pt}
	% Do not indent paragraphs inside lists.
	\setlist*{listparindent=0mm}
	\setenumerate[1]{label=\ensuremath{(\roman*)}, leftmargin=*}
	\setenumerate[2]{label=(\alph*)}
	\setitemize[2]{label=$\triangleright$}
	% Provide enumeratearabic, enumeratelatin, enumerateroman, enumerategreek
	% environments.
	\newlist{enumeratearabic}{enumerate}{1}   \setlist[enumeratearabic]{label=(\arabic*), leftmargin=*}
	\newlist{enumeratearabic*}{enumerate*}{1} \setlist[enumeratearabic*]{label=(\arabic*)}
	\newlist{enumeratelatin}{enumerate}{1}    \setlist[enumeratelatin]{label=(\alph*), leftmargin=*}
	\newlist{enumeratelatin*}{enumerate*}{1}  \setlist[enumeratelatin*]{label=(\alph*)}
	\newlist{enumerateroman}{enumerate}{1}    \setlist[enumerateroman]{label=\ensuremath{(\roman*)}, leftmargin=*}
	\newlist{enumerateroman*}{enumerate*}{1}  \setlist[enumerateroman*]{label=\ensuremath{(\roman*)}}
	\@ifpackageloaded{moreenum}{%
		\newlist{enumerategreek}{enumerate}{1}  \setlist[enumerategreek]{label=(\greek*), leftmargin=*}
	}{%
		\AddEnumerateCounter{\greek}{\@greek}{$\omega$}
		\newlist{enumerategreek}{enumerate}{1}  \setlist[enumerategreek]{label=(\greek*)}
	}
	% Configure the enumerated lists counters for cleveref.
	\@ifpackageloaded{cleveref}{%
		\crefname{enumi}{\GetTranslation{item}}{\GetTranslation{items}}
		\Crefname{enumi}{\GetTranslation{Item}}{\GetTranslation{Items}}
		\crefname{enumeratearabici}{\GetTranslation{item}}{\GetTranslation{items}}
		\Crefname{enumeratearabici}{\GetTranslation{Item}}{\GetTranslation{Items}}
		\crefname{enumeratelatini}{\GetTranslation{item}}{\GetTranslation{items}}
		\Crefname{enumeratelatini}{\GetTranslation{Item}}{\GetTranslation{Items}}
		\crefname{enumerateromani}{\GetTranslation{item}}{\GetTranslation{items}}
		\Crefname{enumerateromani}{\GetTranslation{Item}}{\GetTranslation{Items}}
		\crefname{enumerategreeki}{\GetTranslation{item}}{\GetTranslation{items}}
		\Crefname{enumerategreeki}{\GetTranslation{Item}}{\GetTranslation{Items}}
	}{}
}{}

% Configure the cleveref package.
\@ifpackageloaded{cleveref}{%
	% Define a crefformat for footnotes.
	% The implementation uses ideas from
	% https://tex.stackexchange.com/questions/10102/10116#10116
	\crefformat{footnote}{#2\footnotemark[#1]#3}
}{}

% Configure the url package.
\setboolean{scoop@nextbranch}{false}
\@ifpackageloaded{url}{%
	\setboolean{scoop@nextbranch}{true}
	\@ifclassloaded{jnsao}{\setboolean{scoop@nextbranch}{false}}{}
	\@ifclassloaded{w-art}{\setboolean{scoop@nextbranch}{false}}{}
	\@ifclassloaded{WileyNJD-v2}{\setboolean{scoop@nextbranch}{false}}{}
	\@ifpackageloaded{scrletter}{\setboolean{scoop@nextbranch}{false}}{}
	\ifthenelse{\boolean{scoop@nextbranch}}{%
		% Fix the spacing of // (as in https://) and length of underscore in \url.
		% The implementation uses ideas from
		% http://www.joachim-breitner.de/blog/519-Nicer_URL_formatting_in_LaTeX,
		% http://anti.teamidiot.de/nei/2009/09/latex_url_slash_spacingkerning/.
		\let\UrlSpecialsOld\UrlSpecials
		\def\UrlSpecials{\UrlSpecialsOld\do\/{\Url@slash}\do\_{\Url@underscore}}%
		\def\Url@slash{\@ifnextchar/{\kern-.11em\mathchar47\kern-.2em}%
		{\kern-.0em\mathchar47\kern-.08em\penalty\UrlBigBreakPenalty}}
		\def\Url@underscore{\nfss@text{\leavevmode \kern.06em\vbox{\hrule\@width.3em}}}
		% We often cannot issue \PassOptionsToPackage{lowtilde}{url} early enough.
		% For some reason, it would need to go before \documentclass, which is
		% impractical. So we mimick its behavior here.
		\def\UrlTildeSpecial{\do\~{\raise.2ex\hbox{\m@th$\scriptstyle\sim$}}}
		\let\Url@force@Tilde\UrlTildeSpecial
	}{}
}

% Configure the chngcntr package.
% For the scoop-thesis class, number equations, figures, tables within each chapter.
% For other classes, number them within each section.
\@ifpackageloaded{chngcntr}{%
	\@ifclassloaded{scoop-thesis}{%
		\ifltxcounter{chapter}{%
			% \ifltxcounter{algorithm}{\counterwithin{algorithm}{chapter}}{}
			\ifltxcounter{equation}{\counterwithin{equation}{chapter}}{}
			\ifltxcounter{figure}{\counterwithin{figure}{chapter}}{}
			\ifltxcounter{table}{\counterwithin{table}{chapter}}{}
		}{}
	}{%
		\ifltxcounter{section}{%
			% \ifltxcounter{algorithm}{\counterwithin{algorithm}{section}}{}
			\ifltxcounter{equation}{\counterwithin{equation}{section}}{}
			\ifltxcounter{figure}{\counterwithin{figure}{section}}{}
			\ifltxcounter{table}{\counterwithin{table}{section}}{}
		}{}
	}
}{}

% Configure the algpseudocode package.
\AtBeginDocument{%
	\@ifpackageloaded{algpseudocode}{%
		\algrenewcommand{\algorithmicrequire}{\textbf{\GetTranslation{Input}:}}
		\algrenewcommand{\algorithmicensure}{\textbf{\GetTranslation{Output}:}}
		\algrenewcommand{\algorithmiccomment}[1]{\hfill{\textcolor{TolMutedWine}{/\!/~#1}}}
	}{}
}

% Configure the algorithmic package.
\AtBeginDocument{%
	\@ifpackageloaded{algorithmic}{%
		\renewcommand{\algorithmicrequire}{\textbf{\GetTranslation{Input}:}}
		\renewcommand{\algorithmicensure}{\textbf{\GetTranslation{Output}:}}
		\renewcommand{\algorithmiccomment}[1]{\hfill /\!/~{#1}}
	}{}
}
