% This LaTeX package provides a number of commands useful for teaching
% purposes. It is intended to be used with scoop-lecture-notes.cls,
% scoop-exercises.cls etc.
\ProvidesPackage{scoop-teaching}

% Resolve the dependencies of this package.
\RequirePackage{chngcntr}
\RequirePackage{environ}
\RequirePackage{ifthen}
\RequirePackage{isodate}
\RequirePackage{xcolor}
\PassOptionsToPackage{breakable}{tcolorbox}
\RequirePackage{tcolorbox}

% Define Boolean switches and set their default values.
\provideboolean{scoop@nextbranch}
\provideboolean{scoop@showextras}    \setboolean{scoop@showextras}{true}
\provideboolean{scoop@showsolutions} \setboolean{scoop@showsolutions}{true}
\provideboolean{scoop@showfilenames} \setboolean{scoop@showfilenames}{false}

% Resolve further dependencies of this package.
\setboolean{scoop@nextbranch}{false}
\@ifclassloaded{beamer}{}{\setboolean{scoop@nextbranch}{true}}
\ifthenelse{\boolean{scoop@nextbranch}}{%
	\RequirePackage{marginnote}
	\RequirePackage{titletoc}
	\PassOptionsToPackage{obeyFinal}{todonotes}
	\RequirePackage{todonotes}
}{}
\RequirePackage{scoop-translations}

% Declare the options for this style file.
% The class option 'final' implies noextras, nosolutions.
\DeclareOption{final}{\ExecuteOptions{noextras,nosolutions}}

% The class option 'noextras' produces a version free from extra material,
% to-do notes etc.
\DeclareOption{noextras}{%
	\setboolean{scoop@showextras}{false}
	\PassOptionsToPackage{disable}{todonotes}
}

% The class option 'noextras' produces a version without solutions to
% exercises.
\DeclareOption{nosolutions}{%
	\setboolean{scoop@showsolutions}{false}
}

% The class option 'showfilenames' shows file names (inputAsProblem, inputAsHomeworkProblem).
\DeclareOption{showfilenames}{%
	\setboolean{scoop@showfilenames}{true}
}

% Process all options.
\ProcessOptions\relax

%
% Settings related to lecture notes.
%

% Set the depth for the table of contents.
\setcounter{tocdepth}{2}

% Adjust the formatting of the table of contents through titletoc.sty.
\@ifpackageloaded{titletoc}{%
	\titlecontents{section}[3.3em]{\addvspace{1ex}}{\contentslabel[§~\thecontentslabel]{2.3em}}{\hspace*{-2.3em}}{\titlerule*[1pc]{}\contentspage}
	\titlecontents{subsection}[6.3em]{\addvspace{1ex}}{\contentslabel[§~\thecontentslabel]{3.3em}}{\hspace*{-2.3em}}{\titlerule*[1pc]{}\contentspage}
	\titlecontents{subsubsection}[9.3em]{\addvspace{1ex}}{\contentslabel[§~\thecontentslabel]{3.3em}}{\hspace*{-2.3em}}{\titlerule*[1pc]{}\contentspage}
	\titlecontents{figure}[3.3em]{\addvspace{1ex}}{\contentslabel{2.3em}}{\hspace*{-2.3em}}{\titlerule*[1pc]{}\contentspage}
}{}

% Update the section names etc. for cleveref.sty.
\@ifpackageloaded{cleveref}{%
	\AtBeginDocument{\crefname{section}{§}{§§}}
	\AtBeginDocument{\Crefname{section}{§}{§§}}
	\AtBeginDocument{\crefname{subsection}{§}{§§}}
	\AtBeginDocument{\Crefname{subsection}{§}{§§}}
	\AtBeginDocument{\crefname{subsubsection}{§}{§§}}
	\AtBeginDocument{\Crefname{subsubsection}{§}{§§}}
}

% Define a number of commands useful primarily for lecture notes.
% Define the \alert command to highlight, e.g., important parts of a formula
\providecommand{\alert}[1]{%
	\@ifundefinedcolor{TolVibrantRed}{\textcolor{red}{#1}}{\textcolor{TolVibrantRed}{#1}}%
}

% Define the \EndOfClass command to set an end-of-class marker (with the # of
% class meeting).
\newcommand{\EndOfClass}[1]{\marginnote{\scriptsize \GetTranslation{End of Class} #1}\noindent\makebox[\linewidth]{\rule{\paperwidth}{0.4pt}}}

% Define the \EndOfWeek command to set an end-of-class marker (with the # of
% week).
\newcommand{\EndOfWeek}[1]{\marginnote{\scriptsize \GetTranslation{End of Week} #1}\noindent\makebox[\linewidth]{\rule{\paperwidth}{0.4pt}}}

% Define the \scoopQuestion command to formulate a question (which may be used
% to motivate the following material).
\newcommand{\scoopQuestion}[1]{\textbf{\GetTranslation{Question}:} #1}

% Initialize quiz counter, numbered per section.
\newcounter{scoop@quiz}
\setcounter{scoop@quiz}{0}
\numberwithin{scoop@quiz}{section}
% Define the \scoopQuiz command to formulate a question (for students to
% read or to ask in class). It advances the quiz counter.
\newcommand{\scoopQuiz}[1]{%
	\refstepcounter{scoop@quiz}%
	\textbf{\GetTranslation{Quiz}~\thescoop@quiz:} #1%
}

% Define the scoopExpert environment intended for further information.
\newtcolorbox{scoopExpert}[2][]{%
	breakable,
	parbox = false,
	title = {\hypersetup{allcolors = white}\GetTranslation{Expert Knowledge}: #2},
	#1,
}

% Define the \scoopNote command to formulate a note (for the lecturer to point
% out).
\NewEnviron{scoopNote}{%
	\textbf{\GetTranslation{Note}:} \BODY
}
% Define the \scoopHint command to formulate a hint (to help students).
\newcommand{\scoopHint}[1]{\textbf{\GetTranslation{Hint}:} #1}

% Define the \scoopAnswer command to answer such questions (not typeset when
% noextras option is active).
\ifthenelse{\boolean{scoop@showextras}}{%
	\newcommand{\scoopAnswer}[1]{\textbf{\GetTranslation{Answer}:} #1}
}{%
	\newcommand{\scoopAnswer}[1]{}
}

% Define the \skipline command to provide a unified way to end the first line
% in a theorem-like environment, independently of whether or not an
% itemize-like environment follows immediately.
\newcommand{\skipline}{\hfill \par\vspace{-0.5\baselineskip}}

% Define the scoopExtra environment to typeset material only when the
% 'noextras' option is active. This is meant to be used to typeset material
% exclusive to the lecturer.
\NewEnviron{scoopExtra}{%
	\ifthenelse{\boolean{scoop@showextras}}{%
		\@ifundefinedcolor{TolDarkCyan}{\color{green}}{\color{TolDarkCyan}}\BODY%
	}{}%
}


%
% Settings related to exercise problems.
%

% Set new counters for exercise sheet numbers.
\newcounter{scoop@exercisesheet}
\setcounter{scoop@exercisesheet}{0}
\newcounter{scoop@subclassnr}
\setcounter{scoop@subclassnr}{0}
\newcounter{subexnr}

% Initialize problem counters. By default, problems and homework problems are
% numbered per section.
\newcounter{scoop@problem}
\setcounter{scoop@problem}{0}
\numberwithin{scoop@problem}{section}
\newcounter{scoop@homeworkproblem}
\setcounter{scoop@homeworkproblem}{0}
\numberwithin{scoop@homeworkproblem}{section}

% Make all homework numbers display with corresponding exercise sheet numbers.
\AtEndPreamble{
	\ifthenelse{\equal{\thescoop@subclassnr}{0}}
	{
		\renewcommand{\thescoop@homeworkproblem}
		{\thescoop@exercisesheet.\arabic{scoop@homeworkproblem}}
	}
	{
		\renewcommand{\thescoop@homeworkproblem}
		{\Roman{scoop@subclassnr}-\thescoop@exercisesheet.\arabic{scoop@homeworkproblem}}
	}
}
\AtEndPreamble{\renewcommand{\thescoop@problem}{\arabic{scoop@problem}}}

% Configure the exercisepoints layout.
\RequirePackage[customlayout]{exercisepoints}
\setitempointsunit{\text{\protect\GetTranslation{Point}}}{\text{\protect\GetTranslation{Points}}}


% \inputAsProblem advances the problem counter, prints the problem number and
% optionally the problem's file name and inputs the problem file inside an
% exercise environment, which provides functionality for computing exercise
% points.
\newcommand{\inputAsProblem}[1]{%
	\refstepcounter{scoop@problem}%
	\label{#1}%
	\noindent\textbf{\GetTranslation{Problem}~\thescoop@problem.}
	\ifthenelse{\boolean{scoop@showfilenames}}{\marginnote{\scriptsize\file{#1}}}{}
	\begin{exercise}[]
		\input{#1}
	\end{exercise}
}

% \inputAsHomeworkProblem advances the homework problem counter, prints the
% homework problem number and optionally the homework problem's file name and
% inputs the homework problem file inside an exercise environment, which
% provides functionality for computing exercise points.
\newcommand{\inputAsHomeworkProblem}[1]{%
	\refstepcounter{scoop@homeworkproblem}%
	\label{#1}%
	\noindent\textbf{\GetTranslation{Homework Problem}~\thescoop@homeworkproblem}
	\ifthenelse{\boolean{scoop@showfilenames}}{\marginnote{\scriptsize\file{#1}}}{}
	\begin{exercise}[]
		\input{#1}
	\end{exercise}
}

% Define the scoopProblem problem environment with one optional argument,
% which should contain a brief description of the problem's topic.
% See https://tex.stackexchange.com/questions/33823
% for the test whether #1 is non-empty.
% Maybe supress output of points for problems instead of homework problems.
\newenvironment{scoopProblem}[1][]{%
	\if\relax\detokenize{#1}\relax\else (#1) \fi
	\hfill
	% Check if there are at least two subexercises present in the current
	% exercise.
	\ifcsname exercisepoints@points@\currentexercisenumber.1\endcsname
	% Print the first subexercise points.
	\getsubpoints{\currentexercisenumber.0}
	% Set a counter for subexercises to 1.
	\setcounter{subexnr}{1}%
	\makeatletter%
	% Loop over subexercises assuming there are no more than 15, printing a plus
	% and their points if they exist.
	\@whilenum\value{subexnr}<15\do
	{%
		\makeatother
		\ifcsname exercisepoints@points@\currentexercisenumber.\thesubexnr\endcsname%
		\makeatletter
		%
		+
		\getsubpoints{\currentexercisenumber.\thesubexnr}
		\fi%
		\stepcounter{subexnr}%
	}
	=
	\makeatother%
	\fi
	%
	\getpoints{\currentexercisenumber}~\protect\GetTranslation{Points} %
	\\[0.5\baselineskip]
}{\vspace{\parskip}\par}

% Make the scoopProblem environment known to cleveref.
\@ifpackageloaded{cleveref}{%
	\crefname{scoop@problem}{\GetTranslation{problem}}{\GetTranslation{problems}}
	\Crefname{scoop@problem}{\GetTranslation{Problem}}{\GetTranslation{Problems}}
	\crefname{scoop@homeworkproblem}{\GetTranslation{homework problem}}{\GetTranslation{homework problems}}
	\Crefname{scoop@homeworkproblem}{\GetTranslation{Homework problem}}{\GetTranslation{Homework problems}}
}{}

% Define the scoopSolution environment used to typeset the solution of
% problems, depending on the value of scoop@showsolutions.
\NewEnviron{scoopSolution}{%
	\ifthenelse{\boolean{scoop@showsolutions}}{%
		\noindent\textbf{\GetTranslation{Solution}.}%
		\\[0.5\baselineskip]
		\BODY%
		\vspace{\parskip}\par%
	}{}
}

% Configure a new task environment for double column enumerated tasks.
\@ifpackageloaded{tasks}{%
	\NewTasksEnvironment[label = (\roman*), column-sep = 10pt, before-skip = -2\baselineskip, after-skip = -\baselineskip, after-item-skip = 0pt, label-offset = 10pt ]{scoopFourTask}(4)

	\NewTasksEnvironment[label = (\roman*), column-sep = 10pt, before-skip = -2\baselineskip, after-skip = -\baselineskip, after-item-skip = 0pt, label-offset = 10pt ]{scoopTask}(3)

	\NewTasksEnvironment[label = (\roman*), column-sep = 10pt, before-skip = -2\baselineskip, after-skip = -\baselineskip, after-item-skip = 0pt, label-offset = 10pt ]{scoopTwoTask}(2)
}{}

