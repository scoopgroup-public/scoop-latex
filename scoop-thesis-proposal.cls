% This LaTeX class is intended to be used for thesis proposals.
\PassOptionsToPackage{fit}{truncate} \ProvidesClass{scoop-thesis-proposal}

% Pass all non-implemented options to the base class scrartcl.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}

% Process all options
\ProcessOptions\relax

% Load the base class scrartcl with default documentclass option english
% The implementation uses ideas from
% https://tex.stackexchange.com/questions/147243/a-class-with-default-language-settings.
\RequirePackage{etoolbox}
\preto\@classoptionslist{english,}
\LoadClass[a4paper,DIV=12,parskip=full]{scrartcl}

% Resolve the dependencies of this class.
\RequirePackage{scoop-cls-commons}
\RequirePackage{scoop-translations}

% Configure the document class specific settings.
% Set document type.
\def\scoop@documenttype{\GetTranslation{Thesis Proposal}}

% Remove \@date and spacing following it from \@maketitle
% https://tex.stackexchange.com/questions/288800/omit-the-date-in-maketitle-without-blanking-the-date
\patchcmd{\@maketitle}{{\usekomafont{date}{\@date \par}}\vskip \z@ \@plus 1em}{}{}{}

% Set up the page header and footer.
\if@twoside
	\rohead[]{\truncate{1.0\textwidth}{\ifx\scoop@shorttitle\empty{}\else{\scoop@shorttitle}\fi}}
	\lehead[]{\truncate{1.0\textwidth}{\scoop@shortauthor}}
	\refoot[]{\@date}
	\cofoot[]{\scoop@license}
	\ofoot[{\GetTranslation{page}~\thepage~\GetTranslation{of}~\lastpageref*{LastPage}}]{\GetTranslation{page}~\thepage~\GetTranslation{of}~\lastpageref*{LastPage}}
\else
	\ohead[]{\truncate{0.45\textwidth}{\ifx\scoop@shorttitle\empty{}\else{\scoop@shorttitle}\fi}}
	\ihead[]{\truncate{0.45\textwidth}{\scoop@shortauthor}}
	\ofoot*{\GetTranslation{page}~\thepage~\GetTranslation{of}~\lastpageref*{LastPage}}
	\cfoot[]{\scoop@license}
	\ifoot*{\@date}
\fi

