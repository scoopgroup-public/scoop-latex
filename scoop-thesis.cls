% This LaTeX class is intended to be used for M.Sc. and B.Sc. theses.
\PassOptionsToPackage{fit}{truncate}
\ProvidesClass{scoop-thesis}

% Declare the options for the class.
% The class option 'lineno' turns on line numbering.
\DeclareOption{lineno}{\AtBeginDocument{\linenumbers}}

% Pass all non-implemented options to the base class scrreprt.
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrreprt}}

% Process all options
\ProcessOptions\relax

% Load the base class scrreprt with default documentclass option english.
% The implementation uses ideas from
% https://tex.stackexchange.com/questions/147243/a-class-with-default-language-settings.
\RequirePackage{etoolbox}
\preto\@classoptionslist{english,}
\LoadClass[a4paper,DIV=12,parskip=full,headings=optiontohead]{scrreprt}

% Resolve the dependencies of this package.
\RequirePackage{scoop-cls-commons}
\RequirePackage{scoop-translations}
\RequirePackage{lineno}

% Configure the document class specific settings.
% Set document type.
\def\scoop@documenttype{\GetTranslation{Thesis}}

% Set up additional basic document information exclusive to the 'scoop-thesis'
% document type.
\let\scoop@thesis\empty
\let\scoop@degree\empty
\let\scoop@studentID\empty
\let\scoop@submissiondate\empty
\let\scoop@firstsupervisor\empty
\let\scoop@secondsupervisor\empty
\let\scoop@supervisornote\empty

% Define some commands to manipulate this basic information.
\newcommand{\thesis}[1]{\gdef\scoop@thesis{#1}}
\newcommand{\degree}[1]{\gdef\scoop@degree{#1}}
\newcommand{\studentID}[1]{\gdef\scoop@studentID{#1}}
\newcommand{\submissiondate}[1]{\gdef\scoop@submissiondate{#1}}
\newcommand{\firstsupervisor}[1]{\gdef\scoop@firstsupervisor{#1}}
\newcommand{\secondsupervisor}[1]{\gdef\scoop@secondsupervisor{#1}}
\newcommand{\supervisornote}[1]{\gdef\scoop@supervisornote{#1}}

% Set up the appearance of author, date, title, and section headings etc.
\renewcommand*{\sectfont}{\color{TolMutedBlue}\sffamily}
\setkomafont{author}{\large\normalfont\sffamily}
\setkomafont{date}{\large\normalfont\sffamily}
\setkomafont{title}{\color{TolMutedBlue}\normalfont\sffamily\scshape}
\addtokomafont{disposition}{\scshape}
\addtokomafont{subsubsection}{\scshape\itshape}

% Remove \@date and spacing following it from \@maketitle. The implementation
% uses ideas from https://tex.stackexchange.com/questions/288800.
\patchcmd{\@maketitle}{{\usekomafont{date}{\@date \par}}\vskip \z@ \@plus 1em}{}{}{}

% Set up the page header and footer.
\if@twoside
	\refoot[]{\truncate{1.0\textwidth}{\@author}}
	\lofoot[]{\truncate{1.0\textwidth}{\ifx\scoop@shorttitle\empty{\@title}\else{\scoop@shorttitle}\fi}}
	\ofoot[{\GetTranslation{page}~\thepage~\GetTranslation{of}~\lastpageref*{LastPage}}]{\GetTranslation{page}~\thepage~\GetTranslation{of}~\lastpageref*{LastPage}}
\else
	\chead[]{\truncate{0.45\textwidth}{\ifx\scoop@shorttitle\empty{\@title}\else{\scoop@shorttitle}\fi}}
	\ifoot[]{\truncate{0.45\textwidth}{\@author}}
	\ofoot*{\GetTranslation{page}~\thepage~\GetTranslation{of}~\lastpageref*{LastPage}}
\fi
\cfoot*{}

% Configure the appearance of the title page.
% https://www.informatik.uni-heidelberg.de/images/perSemester/2019WS/2019-Leitfaden-zur-Erstellung-der-Bachelorarbeit.pdf
\renewcommand{\maketitle}{%
	\begin{titlepage}
		\begin{center}
			Ruprecht-Karls-Universität Heidelberg \\
			Fakultät für Mathematik und Informatik \\
			Institut für Mathematik \\
		\end{center}
		\begin{center}
			{\LARGE\scoop@thesis{}}\\
			\vspace{1cm}
			{\huge\@title{}}\\
		\end{center}
		\vfill
		\begin{tabular}[b]{p{0.33\textwidth}l}
			Name: & \@author\\%
			\GetTranslation{Student ID}: & \scoop@studentID\\%
			\GetTranslation{Betreuung}: & \scoop@firstsupervisor\\%
			& \scoop@secondsupervisor\\%
			& \scoop@supervisornote\\
			\GetTranslation{Date of Submission}: & \scoop@submissiondate
		\end{tabular}
	\end{titlepage}
}
